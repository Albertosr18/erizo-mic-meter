import {Component, OnInit} from '@angular/core';
import * as Tone from "tone";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  public ocultarButton: boolean = false;
  public ocultarLoad: boolean = true;
  title = 'Erizo-mic-meter';
  mediaStreamSource: MediaStreamAudioSourceNode | null = null;
  analyser: AnalyserNode | null = null;
  minDecibels = 0; // Rango mínimo de decibelios
  maxDecibels = 100; // Rango máximo de decibelios
  decibelPercentage: number | null = null;
  public gaugeThick = 50;
  public gaugethresholds = {
    '0': { color: 'red' },
    '30': { color: 'orangeRed' },
    '50': { color: 'orange' },
    '60': { color: 'green' },
  };
  public gaugemax = 100;
  private isRecording = false;
  private measurements: number[] = [];
  minParam: number = -100;
  maxParam : number = -5;


  ngOnInit() {

  }

  startRecording() {
    this.ocultarButton = true;
    this.ocultarLoad = false;
    this.isRecording = true;

    const meter = new Tone.Meter();
    const mic = new Tone.UserMedia();
    mic.open();
    mic.connect(meter);
    const interval = setInterval(() => {
      if(meter.getValue()!== -Infinity){
        this.measurements.push(<number>meter.getValue());
        console.log("interval:", meter.getValue());
      }
    }, 100);

    setTimeout(() => {
      clearInterval(interval);
      // Detener el micrófono
      mic.close();
      console.log("Micrófono cerrado.");
      // Calcular la media de las mediciones
      const sum = this.measurements.reduce((acc, val) => acc + val, 0);
      const average = sum / this.measurements.length;
      console.log(`Media de las mediciones: ${average}`);
      const normalizedValue = (average - this.minDecibels) / (this.maxDecibels - this.minDecibels);

      // Asegurarse de que el porcentaje esté en el rango [0, 100]
      const rangoMinimo = this.minParam;
      const rangoMaximo = this.maxParam;
      const rangoCompleto = rangoMaximo - rangoMinimo;
      const valorDentroDelRango = Math.max(0, Math.min(average - rangoMinimo, rangoCompleto));
      this.decibelPercentage = Math.trunc((valorDentroDelRango / rangoCompleto) * 100);
      console.log("Porcentaje:", rangoMinimo);
      console.log("Mediciones capturadas:", Math.pow(10, normalizedValue / 10));

      this.ocultarLoad = true;
    }, 10000);

  }

   restart() {
     this.decibelPercentage=null;
     this.ocultarButton = false;
  }
}
