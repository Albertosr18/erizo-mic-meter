import {NgModule} from '@angular/core';
import {PlatformModule} from '@angular/cdk/platform';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgxGaugeModule} from 'ngx-gauge';
import { FormsModule } from '@angular/forms'; // Importa FormsModule desde '@angular/forms'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PlatformModule,
    AppRoutingModule,
    FormsModule,
    NgxGaugeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
